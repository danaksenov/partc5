package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Задайте строку: ");
        String text = in.nextLine();
        System.out.println("\n\n" + formatText(text));
    }
    public static String formatText(String incomingText) {
        StringBuilder sb = new StringBuilder();
        String[] strArr = incomingText.split(" ");
        for (int i = 0; i < strArr.length; i++) {

            if ((strArr[i].length() % 5 != 0) && !isVowel(strArr[i])) {
                sb.append(strArr[i]).append(" ");
            }
        }

        String outText = sb.toString().trim();
        return outText;
    }

    public static boolean isVowel(String incomingText) {
        switch (incomingText.charAt(0)) {
            case 'б':
            case 'в':
            case 'г':
            case 'д':
            case 'ж':
            case 'з':
            case 'й':
            case 'л':
            case 'м':
            case 'н':
            case 'р':
            case 'к':
            case 'п':
            case 'с':
            case 'т':
            case 'ф':
            case 'х':
            case 'ч':
            case 'ц':
            case 'ш':
            case 'щ':
                return true;
            default:
                return false;

        }
    }
}
